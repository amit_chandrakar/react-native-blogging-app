export const ENTRIES1 = [
    {
        title: 'India crush Sri Lanka by 10 wickets to win eighth Asia Cup cricket crown',
        subtitle: 'Sri Lanka were bowled all-out for just 50 runs, as India’s Mohammed Siraj claiming a career-best 6-21 – including four wickets in one over.',
        illustration: 'https://www.aljazeera.com/wp-content/uploads/2023/09/AP23260385213604-1694947523.jpg?resize=770%2C513&quality=80'
    },
    {
        title: '‘Deeply touched’ by outpouring of wishes on birthday, says PM Modi',
        subtitle: 'The prime minister, who turned 73 on Sunday, had quite a busy day despite the occasion.',
        illustration: 'https://www.hindustantimes.com/ht-img/img/2023/09/17/550x309/IMG-20230917-WA0001_1694938545194_1694964082131.jpg'
    },
    {
        title: 'Back iPhone 15 series launch: Croma, Amazon, Flipkart, Vijay Sales unveil special discounts',
        subtitle: 'Apple fans in India can pre-order the iPhone 15 series from major retailers like Croma, Vijay Sales, Flipkart, and Amazon. Discounts and trade-in incentives are available.',
        illustration: 'https://www.livemint.com/lm-img/img/2023/09/17/600x338/pro_and_pro_max_1694550468379_1694913485085.jpg'
    },
    {
        title: 'Jawan Box Office collection Day 10: SRK’s blockbuster refuses to slow down; check advance booking numbers for Day 11',
        subtitle: 'Jawan Box Office collection Day 10: Shah Rukh Khan\'s movie is still going strong while advance booking numbers are steady.',
        illustration: 'https://wallpaperaccess.com/full/9335211.jpg'
    },
    {
        title: 'Back Xiaomi launches `Pick Mi Up` home service in India: What is it and how much it costs',
        subtitle: 'Xiaomi India introduces Pick Mi Up, a home pick-up service via the Xiaomi Service+ app for Xiaomi and Redmi users, offering smartphone repairs and support features. Meanwhile, Xiaomi Corp. reports better-than-expected profit in the smartphone market.',
        illustration: 'https://www.livemint.com/lm-img/img/2023/09/16/600x338/XIAOMI-RESULTS--1_1694752741556_1694862571873.JPG'
    },
    {
        title: 'Mint Explainer: Why is Akasa Air taking 43 pilots to court?',
        subtitle: 'The airline\'s flight cancellation rate last month was 1.17%, the second-highest since its launch in August 2022 (Photo: Akasa Air)',
        illustration: 'https://images.moneycontrol.com/static-mcnews/2021/12/Akasa-Air-1.jpg?impolicy=website&width=800&height=800'
    }
];
