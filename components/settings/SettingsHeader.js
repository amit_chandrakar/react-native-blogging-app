import React from 'react';
import { View, Text, StyleSheet, useColorScheme } from 'react-native';

function SettingsHeader({ title }) {

  const colorScheme = useColorScheme();

    return (
        <View style={[styles.container, {backgroundColor: colorScheme === 'light' ? '#f4f4f4' : 'black'}]}>
            <Text style={styles.title}>{title}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#f0f0f0',
    },
    title: {
        fontSize: 16,
        paddingLeft: 15,
        fontWeight: '600',
        color: '#666'
    }
});

export default SettingsHeader;
