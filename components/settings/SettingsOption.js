import React from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet, useColorScheme } from 'react-native';

function SettingsOption({ icon, title, onPress, rightElement })
{
  const colorScheme = useColorScheme();


    return (
        <TouchableOpacity onPress={onPress} style={[styles.container, {backgroundColor: colorScheme === 'light' ? '#f4f4f4' : 'black'}]}>
            <Image source={icon} style={styles.icon} />
            <Text style={[styles.title, {color: colorScheme === 'light' ? '#8e8e8e' : '#828181'} ]}>{title}</Text>
            {rightElement}
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 15,
        backgroundColor: '#fff',
    },
    icon: {
        width: 25,
        height: 25,
        marginRight: 15
    },
    title: {
        flex: 1,
        fontSize: 18
    }
});

export default SettingsOption;
