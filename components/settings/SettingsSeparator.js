import React from 'react';
import { View, StyleSheet, useColorScheme } from 'react-native';

function SettingsSeparator()
{
      const colorScheme = useColorScheme();


    return <View style={[styles.separator, {backgroundColor: colorScheme === 'light' ? '#8e8e8e' : '#828181'}]} />;
}

const styles = StyleSheet.create({
    separator: {
        height: 1,
        backgroundColor: '#e0e0e0',
        marginLeft: 55 // to align after the icon
    }
});

export default SettingsSeparator;
