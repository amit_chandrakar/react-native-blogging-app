/**
 * Learn more about deep linking with React Navigation
 * https://reactnavigation.org/docs/deep-linking
 * https://reactnavigation.org/docs/configuring-links
 */
import * as Linking from "expo-linking";

export default {
  prefixes: [Linking.createURL("/")],
  config: {
    screens: {
      Root: {
        screens: {
          TabOne: {
            screens: {
              Home: "one",
            },
          },
          TabTwo: {
            screens: {
              Search: "two",
            },
          },
          TabThree: {
            screens: {
              Bookmark: "three",
            },
          },
          TabFour: {
            screens: {
              Setting: "four",
            },
          },
        },
      },
      NotFound: "*",
    },
  },
};
