import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    header: {
      flexDirection: "row",
      justifyContent: "flex-start",
      alignItems: "center",
      alignContent: "center",
      padding: 10,
    },
    title: {
      backgroundColor: 'transparent',
      color: 'rgba(255, 255, 255, 0.9)',
      fontSize: 18,
      fontWeight: 'bold',
      textAlign: 'center',
    },

    blogResultContainer: {
      flex: 1,
    },
    blogResult: {
      flexDirection: "row",
      justifyContent: "flex-start",
      alignContent: "center",
      padding: 10,
      borderRadius: 10,
    },
    blogResultImage: {
      width: 70,
      height: 70,
      borderRadius: 10,
      marginRight: 10,
    },
    blogResultText: {
      flex: 1,
    },
    blogResultTitle: {
      fontSize: 16,
      fontWeight: 'bold',
    },
    blogResultAuthor: {
      fontSize: 12,
      fontWeight: 'bold',
      marginTop: 5,
    },
    separator: {
      borderBottomWidth: 1,
      marginVertical: 10,
      marginHorizontal: 10,

    },

  });

export default styles;