import { useRef, useEffect } from 'react';
import { SafeAreaView, useColorScheme, Image, ScrollView, TextInput } from "react-native";
import { Text, View } from "../components/Themed";
import styles from "./search.style";
import Icon from 'react-native-vector-icons/Ionicons';

export default function Search({ navigation })
{
  const colorScheme = useColorScheme();
  const textInputRef = useRef(null);

  useEffect(() => {
    // Focus the TextInput when the component loads
    textInputRef.current.focus();
  }, []);

  return (
    <SafeAreaView style={[{backgroundColor: colorScheme === 'light' ? '#f4f4f4' : 'black'}]}>
      <ScrollView showsVerticalScrollIndicator={false}>

        {/* Start: Header section */}
        <View style={[styles.header, {borderBottomColor: colorScheme === 'light' ? '#eaeaea' : '#515151'}]}>
          <Icon name="arrow-back-outline" size={25} color={colorScheme === 'light' ? '#8e8e8e' : 'white'} onPress={ () => navigation.navigate('Root') } />
          <Text style={[styles.title, colorScheme === 'light' ? styles.titleDark : '']} onPress={ () => navigation.navigate('Root') }>Search</Text>
        </View>
        {/* End: Header section */}

        <View>
          <TextInput
            style={styles.searchInput}
            placeholder="Search for posts here..."
            placeholderTextColor={colorScheme === 'dark' ? '#8e8e8e' : 'white'}
            ref={textInputRef} // Assign the ref to the TextInput
          />
        </View>

        <View>
          <Text style={[styles.searchResults, colorScheme === 'light' ? styles.titleDark : '']}>Search Results</Text>
        </View>

        <View style={styles.blogResultContainer}>
          <View style={styles.blogResult}>
            <Image
              source={{ uri: 'https://picsum.photos/300/300' }}
              style={styles.blogResultImage}
            />
            <View style={styles.blogResultText}>
              <Text style={[styles.blogResultTitle]}>Lorem ipsum dolor sit amet, op adipiscing elit</Text>
              <Text style={[styles.blogResultAuthor]}>By John Doe</Text>
            </View>
          </View>
        </View>
        {/* Separator */}
        <View style={[styles.separator, {borderBottomColor: colorScheme === 'light' ? '#eaeaea' : '#515151'}]} />

        <View style={styles.blogResultContainer}>
          <View style={styles.blogResult}>
            <Image
              source={{ uri: 'https://picsum.photos/250/300' }}
              style={styles.blogResultImage}
            />
            <View style={styles.blogResultText}>
              <Text style={[styles.blogResultTitle]}>Lorem ipsum dolor sit amet, op adipiscing elit</Text>
              <Text style={[styles.blogResultAuthor]}>By John Doe</Text>
            </View>
          </View>
        </View>
        {/* Separator */}
        <View style={[styles.separator, {borderBottomColor: colorScheme === 'light' ? '#eaeaea' : '#515151'}]} />

        <View style={styles.blogResultContainer}>
          <View style={styles.blogResult}>
            <Image
              source={{ uri: 'https://picsum.photos/200/200' }}
              style={styles.blogResultImage}
            />
            <View style={styles.blogResultText}>
              <Text style={[styles.blogResultTitle]}>Lorem ipsum dolor sit amet, op adipiscing elit</Text>
              <Text style={[styles.blogResultAuthor]}>By John Doe</Text>
            </View>
          </View>
        </View>
        {/* Separator */}
        <View style={[styles.separator, {borderBottomColor: colorScheme === 'light' ? '#eaeaea' : '#515151'}]} />

        <View style={styles.blogResultContainer}>
          <View style={styles.blogResult}>
            <Image
              source={{ uri: 'https://picsum.photos/250/300' }}
              style={styles.blogResultImage}
            />
            <View style={styles.blogResultText}>
              <Text style={[styles.blogResultTitle]}>Lorem ipsum dolor sit amet, op adipiscing elit</Text>
              <Text style={[styles.blogResultAuthor]}>By John Doe</Text>
            </View>
          </View>
        </View>
        {/* Separator */}
        <View style={[styles.separator, {borderBottomColor: colorScheme === 'light' ? '#eaeaea' : '#515151'}]} />

        <View style={styles.blogResultContainer}>
          <View style={styles.blogResult}>
            <Image
              source={{ uri: 'https://picsum.photos/260/300' }}
              style={styles.blogResultImage}
            />
            <View style={styles.blogResultText}>
              <Text style={[styles.blogResultTitle]}>Lorem ipsum dolor sit amet, op adipiscing elit</Text>
              <Text style={[styles.blogResultAuthor]}>By John Doe</Text>
            </View>
          </View>
        </View>
        {/* Separator */}
        <View style={[styles.separator, {borderBottomColor: colorScheme === 'light' ? '#eaeaea' : '#515151'}]} />

        <View style={styles.blogResultContainer}>
          <View style={styles.blogResult}>
            <Image
              source={{ uri: 'https://picsum.photos/270/300' }}
              style={styles.blogResultImage}
            />
            <View style={styles.blogResultText}>
              <Text style={[styles.blogResultTitle]}>Lorem ipsum dolor sit amet, op adipiscing elit</Text>
              <Text style={[styles.blogResultAuthor]}>By John Doe</Text>
            </View>
          </View>
        </View>

      </ScrollView>
    </SafeAreaView>
  );
}
