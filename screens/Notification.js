import { useRef, useEffect } from 'react';
import { SafeAreaView, useColorScheme, Image, ScrollView, TextInput } from "react-native";
import { Text, View } from "../components/Themed";
import styles from "./notification.style";
import Icon from 'react-native-vector-icons/Ionicons';

export default function Notification({ navigation })
{
  const colorScheme = useColorScheme();

  useEffect(() => {
    // Focus the TextInput when the component loads
  }, []);

  return (
    <SafeAreaView style={[{backgroundColor: colorScheme === 'light' ? '#f4f4f4' : 'black'}]}>
      <ScrollView showsVerticalScrollIndicator={false}>

        {/* Start: Header section */}
        <View style={[styles.header, {borderBottomColor: colorScheme === 'light' ? '#eaeaea' : '#515151'}]}>
          <Icon name="arrow-back-outline" size={25} color={colorScheme === 'light' ? '#8e8e8e' : 'white'} onPress={ () => navigation.navigate('Root') } />
          <Text style={[styles.title, colorScheme === 'light' ? styles.titleDark : '']} onPress={ () => navigation.navigate('Root') }>Notifications</Text>
        </View>
        {/* End: Header section */}

        <View style={styles.blogResultContainer}>
          <View style={styles.blogResult}>
            <Image
              source={{ uri: 'https://picsum.photos/300/300' }}
              style={styles.blogResultImage}
            />
            <View style={styles.blogResultText}>
              <Text style={[styles.blogResultTitle]}>Lorem ipsum dolor sit amet, op adipiscing elit</Text>
              <Text style={[styles.blogResultAuthor]}>By John Doe</Text>
            </View>
          </View>
        </View>
        {/* Separator */}
        <View style={[styles.separator, {borderBottomColor: colorScheme === 'light' ? '#eaeaea' : '#515151'}]} />

        <View style={styles.blogResultContainer}>
          <View style={styles.blogResult}>
            <Image
              source={{ uri: 'https://picsum.photos/250/300' }}
              style={styles.blogResultImage}
            />
            <View style={styles.blogResultText}>
              <Text style={[styles.blogResultTitle]}>Lorem ipsum dolor sit amet, op adipiscing elit</Text>
              <Text style={[styles.blogResultAuthor]}>By John Doe</Text>
            </View>
          </View>
        </View>
        {/* Separator */}
        <View style={[styles.separator, {borderBottomColor: colorScheme === 'light' ? '#eaeaea' : '#515151'}]} />

        <View style={styles.blogResultContainer}>
          <View style={styles.blogResult}>
            <Image
              source={{ uri: 'https://picsum.photos/200/200' }}
              style={styles.blogResultImage}
            />
            <View style={styles.blogResultText}>
              <Text style={[styles.blogResultTitle]}>Lorem ipsum dolor sit amet, op adipiscing elit</Text>
              <Text style={[styles.blogResultAuthor]}>By John Doe</Text>
            </View>
          </View>
        </View>
        {/* Separator */}
        <View style={[styles.separator, {borderBottomColor: colorScheme === 'light' ? '#eaeaea' : '#515151'}]} />

        <View style={styles.blogResultContainer}>
          <View style={styles.blogResult}>
            <Image
              source={{ uri: 'https://picsum.photos/250/300' }}
              style={styles.blogResultImage}
            />
            <View style={styles.blogResultText}>
              <Text style={[styles.blogResultTitle]}>Lorem ipsum dolor sit amet, op adipiscing elit</Text>
              <Text style={[styles.blogResultAuthor]}>By John Doe</Text>
            </View>
          </View>
        </View>
        {/* Separator */}
        <View style={[styles.separator, {borderBottomColor: colorScheme === 'light' ? '#eaeaea' : '#515151'}]} />

        <View style={styles.blogResultContainer}>
          <View style={styles.blogResult}>
            <Image
              source={{ uri: 'https://picsum.photos/260/300' }}
              style={styles.blogResultImage}
            />
            <View style={styles.blogResultText}>
              <Text style={[styles.blogResultTitle]}>Lorem ipsum dolor sit amet, op adipiscing elit</Text>
              <Text style={[styles.blogResultAuthor]}>By John Doe</Text>
            </View>
          </View>
        </View>
        {/* Separator */}
        <View style={[styles.separator, {borderBottomColor: colorScheme === 'light' ? '#eaeaea' : '#515151'}]} />

        <View style={styles.blogResultContainer}>
          <View style={styles.blogResult}>
            <Image
              source={{ uri: 'https://picsum.photos/270/300' }}
              style={styles.blogResultImage}
            />
            <View style={styles.blogResultText}>
              <Text style={[styles.blogResultTitle]}>Lorem ipsum dolor sit amet, op adipiscing elit</Text>
              <Text style={[styles.blogResultAuthor]}>By John Doe</Text>
            </View>
          </View>
        </View>

      </ScrollView>
    </SafeAreaView>
  );
}
