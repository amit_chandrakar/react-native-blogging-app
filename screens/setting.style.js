import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#f0f0f0',
},
title: {
    fontSize: 16,
    paddingLeft: 15,
    fontWeight: '600',
    color: '#666'
}
});

export default styles;