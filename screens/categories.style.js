import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    alignContent: "center",
    padding: 10,
  },
  container: {

  },
  flexContainer: {
    flexDirection: "row",
    marginHorizontal: 12,
    flexWrap: "wrap",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
  },
  titleDark: {
    color: "white",
  },
  categoryContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
    padding: 5,
    marginTop: 15,
  },
  category: {
    alignItems: "center",
    marginHorizontal: 10,
  },
  categoryImage: {
    width: 170,
    height: 170,
    borderRadius: 10,
  },
  categoryText: {
    marginVertical: 10,
    fontSize: 16,
    marginBottom: 20,
  },
  categoryTextDark: {
    color: "white",
  },
  postContainer: {
    marginTop: 10,
  },
});

export default styles;