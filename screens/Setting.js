import { SafeAreaView, useColorScheme } from "react-native";
import SettingsHeader from "../components/settings/SettingsHeader";
import SettingsOption from "../components/settings/SettingsOption";
import SettingsSeparator from "../components/settings/SettingsSeparator";

export default function Setting() {
  const colorScheme = useColorScheme();

  return (
    <SafeAreaView>

      <SettingsHeader title="Account" />
      <SettingsOption
        icon={{ uri: 'https://cdn-icons-png.flaticon.com/512/4104/4104800.png' }}
        title="Privacy"
        onPress={() => { }}
      />
      <SettingsSeparator />
      <SettingsOption
        icon={{ uri: 'https://www.freeiconspng.com/thumbs/secure-icon-png/internet-security-icon-png-25.png' }}
        title="Security"
        onPress={() => { }}
      />
      <SettingsSeparator />
      <SettingsOption
        icon={{ uri: 'https://icon-library.com/images/theme-icon/theme-icon-5.jpg' }}
        title="Theme"
        onPress={() => { }}
      />
      <SettingsSeparator />
      <SettingsOption
        icon={{ uri: 'https://www.freeiconspng.com/thumbs/share-icon/file-sharing-share-sharing-social-media-icon--28.png' }}
        title="Share App"
        onPress={() => { }}
      />
      {/* Add more settings options as needed... */}

    </SafeAreaView>
  );
}
