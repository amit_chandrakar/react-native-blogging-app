import { SafeAreaView, useColorScheme, Image, ScrollView } from "react-native";
import { Text, View } from "../components/Themed";
import Icon from 'react-native-vector-icons/FontAwesome';
import Carousel from 'react-native-snap-carousel';
import { sliderWidth, itemWidth } from '../styles/SliderEntry.style';
import SliderEntry from '../components/SliderEntry';
import { ENTRIES1 } from '../static/entries';
import { scrollInterpolators, animatedStyles } from '../utils/animations';
import styles from "./blog.style";
import IconBack from 'react-native-vector-icons/Ionicons';

const renderItem = ({ item, index }) => {
  return <SliderEntry data={item} even={(index + 1) % 2 === 0} />;
}

const refNumber = 3;

export default function Blog({navigation})
{
  const colorScheme = useColorScheme();

  return (
    <SafeAreaView style={[{backgroundColor: colorScheme === 'light' ? '#f4f4f4' : 'black'}]}>
      <ScrollView showsVerticalScrollIndicator={false}>

        {/* Start: Header section */}
        <View style={[styles.header, {borderBottomColor: colorScheme === 'light' ? '#eaeaea' : '#515151'}]}>
          <IconBack name="arrow-back-outline" size={20} color={colorScheme === 'light' ? '#8e8e8e' : 'white'} onPress={ () => navigation.navigate('Root') } />
          <Text style={[styles.title, colorScheme === 'light' ? styles.titleDark : '']} onPress={ () => navigation.navigate('Root') }>Back</Text>
        </View>
        {/* End: Header section */}

        {/* Start: Latest Blogs section */}
        <View style={styles.container}>
          <View>
            <Image style={styles.postImage} source={{ uri: 'https://www.hindustantimes.com/ht-img/img/2023/09/17/550x309/Congress-president-Mallikarjun-Kharge-and-former-p_1694958285134.jpg' }} onStartShouldSetResponder={ () => props.navigation.navigate('Blog') } />
            <Text style={styles.postHeading}>Congress leaders from Delhi, Punjab express reservation about poll pact with AAP</Text>

            <Text style={styles.postDescription}>The Congress is likely to contest the 2022 Punjab assembly elections in alliance with the Shiromani Akali Dal (SAD) and the Bahujan Samaj Party (BSP), but the party’s Delhi unit is not in favour of an alliance with the Aam Aadmi Party (AAP) in the national capital, according to people aware of the matter.</Text>
            <Text style={styles.postDescription}>The Congress is likely to contest the 2022 Punjab assembly elections in alliance with the Shiromani Akali Dal (SAD) and the Bahujan Samaj Party (BSP), but the party’s Delhi unit is not in favour of an alliance with the Aam Aadmi Party (AAP) in the national capital, according to people aware of the matter.</Text>
            <Text style={styles.postDescription}>The Congress is likely to contest the 2022 Punjab assembly elections in alliance with the Shiromani Akali Dal (SAD) and the Bahujan Samaj Party (BSP), but the party’s Delhi unit is not in favour of an alliance with the Aam Aadmi Party (AAP) in the national capital, according to people aware of the matter.</Text>
            <Text style={styles.postDescription}>The Congress is likely to contest the 2022 Punjab assembly elections in alliance with the Shiromani Akali Dal (SAD) and the Bahujan Samaj Party (BSP), but the party’s Delhi unit is not in favour of an alliance with the Aam Aadmi Party (AAP) in the national capital, according to people aware of the matter.</Text>

          </View>
        </View>
        {/* End: Latest Blogs section */}

      </ScrollView>
    </SafeAreaView>
  );
}


