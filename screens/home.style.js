import { StyleSheet } from "react-native";
import style, { colors } from '../styles/index.style';

const styles = StyleSheet.create({
    header: {
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      alignContent: "center",
      padding: 10,
    },
    notification: {
      width: 10, // You can set width and height as needed
      height: 10, // Same as width for a perfect circle
      borderRadius: 5, // Half of the width (or height) to create a circle
      backgroundColor: "red", // Example background color
    },
    title: {
      backgroundColor: 'transparent',
      color: 'rgba(255, 255, 255, 0.9)',
      fontSize: 20,
      fontWeight: 'bold',
      textAlign: 'center',
    },
    titleDark: {
      color: colors.black
    },
    appName: {
      fontSize: 25,
    },
    flexContainer: {
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      padding: 10,
    },
    categoryImage: {
      width: 100,
      height: 100,
      borderRadius: 50,
      marginBottom: 10,
    },
    category: {
      alignItems: "center",
      paddingRight: 10,
    },
    categoryContainer: {
      flexDirection: "row",
      padding: 10,
      paddingBottom: 10,
      paddingTop: 10
    },
    postContainer: {
      marginTop: 10,
    },
    post: {
      padding: 10,
    },
    separator: {
      height: 1,
      backgroundColor: '#474747', // Separator color
      marginBottom: 10,
      marginTop: 20,
    },
    postImage: {
      width: 370,
      height: 200,
      marginBottom: 10,
      borderRadius: 10,
    },
    postHeading: {
      fontSize: 15,
      fontWeight: "bold",
      marginBottom: 10,
    },
    viewAll: {
      fontSize: 12,
      color: "#9DB1FA",
    },

    slider: {
        marginTop: 15,
        overflow: 'visible' // for custom animations
    },
    sliderContentContainer: {
        paddingVertical: 10 // for custom animation
    }

  });

export default styles;