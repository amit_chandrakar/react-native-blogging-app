import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
  },
  message: {
    fontSize: 20,
    textAlign: "center",
    marginBottom: 20,
  },
  loginBtn: {
    fontSize: 20,
    textAlign: "center",
    backgroundColor: "#9DB1FA",
    color: "#ffffff",
    padding: 10,
  },
});

export default styles;