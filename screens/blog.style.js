import { StyleSheet } from "react-native";
import style, { colors } from '../styles/index.style';

const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    alignContent: "center",
    padding: 10,
  },
  container: {
    padding: 10,
  },
  categoryImage: {
    width: 100,
    height: 100,
    borderRadius: 50,
    marginBottom: 10,
  },
  postImage: {
    width: '100%',
    height: 200,
    marginBottom: 10,
    borderRadius: 10,
  },
  postHeading: {
    fontSize: 15,
    fontWeight: "bold",
    marginBottom: 10,
  },
  postDescription: {
    fontSize: 15,
    marginBottom: 10,
    textAlign: "justify",
  },

});

export default styles;