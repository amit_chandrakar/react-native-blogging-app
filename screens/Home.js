import { SafeAreaView, useColorScheme, Image, ScrollView } from "react-native";
import { Text, View } from "../components/Themed";
import Icon from 'react-native-vector-icons/FontAwesome';
import Carousel from 'react-native-snap-carousel';
import { sliderWidth, itemWidth } from '../styles/SliderEntry.style';
import SliderEntry from '../components/SliderEntry';
import { ENTRIES1 } from '../static/entries';
import { scrollInterpolators, animatedStyles } from '../utils/animations';
import styles from "./home.style";
import { TouchableOpacity } from "react-native-gesture-handler";

const renderItem = ({ item, index }) => {
  return <SliderEntry data={item} even={(index + 1) % 2 === 0} />;
}

const refNumber = 3;

export default function Home(props)
{
  const colorScheme = useColorScheme();

  return (
    <SafeAreaView style={[{backgroundColor: colorScheme === 'light' ? '#f4f4f4' : 'black'}]}>
      <ScrollView showsVerticalScrollIndicator={false}>

        {/* Start: Header section */}
        <View style={[styles.header, {borderBottomColor: colorScheme === 'light' ? '#eaeaea' : '#515151'}]}>
          <Text style={[styles.title, styles.appName, colorScheme === 'light' ? styles.titleDark : '']}>Gossip Line</Text>
          <Text onPress={ () => props.navigation.navigate('Notification') } >
            <Icon name="bell" size={20} color={colorScheme === 'light' ? '#8e8e8e' : 'white'} />
            <View style={styles.notification}></View>
            {/* <Text style={[styles.notification, {color: colorScheme === 'light' ? 'white' : 'white', fontSize: 9 }]}>0</Text> */}
          </Text>
        </View>
        {/* End: Header section */}

        {/* Start: Slider section */}
        <View style={{ flex: 1 }}>
            <Carousel
              data={ENTRIES1}
              renderItem={renderItem}
              sliderWidth={sliderWidth}
              itemWidth={itemWidth}
              containerCustomStyle={styles.slider}
              contentContainerCustomStyle={styles.sliderContentContainer}
              scrollInterpolator={scrollInterpolators[`scrollInterpolator${refNumber}`]}
              slideInterpolatedStyle={animatedStyles[`animatedStyles${refNumber}`]}
              useScrollView={true}
              autoplay={true}
              loop={true}
              autoplayDelay={500}
              autoplayInterval={3000}
            />
        </View>
        {/* End: Slider section */}

        {/* Start: Categories section */}
        <View style={{ flex: 1, marginTop: 10 }}>
          <View style={styles.flexContainer}>
            <Text style={[styles.title, colorScheme === 'light' ? styles.titleDark : '']}>Categories</Text>
            <Text style={styles.viewAll} onPress={ () => props.navigation.navigate('Categories') }>View All</Text>
          </View>

          <View style={styles.categoryContainer}>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              <View style={styles.category}>
                <Image style={styles.categoryImage} source={{ uri: 'https://wallpaperaccess.com/full/552032.jpg' }} />
                <Text>Sports</Text>
              </View>
              <View style={styles.category}>
                <Image style={styles.categoryImage} source={{ uri: 'https://e0.pxfuel.com/wallpapers/946/767/desktop-wallpaper-html-background-code-coding-tags-thumbnail.jpg' }} />
                <Text>Coding</Text>
              </View>
              <View style={styles.category}>
                <Image style={styles.categoryImage} source={{ uri: 'https://e1.pxfuel.com/desktop-wallpaper/534/511/desktop-wallpaper-political-science.jpg' }} />
                <Text>Politics</Text>
              </View>
              <View style={styles.category}>
                <Image style={styles.categoryImage} source={{ uri: 'https://wallpapercave.com/wp/wp9283871.jpg' }} />
                <Text>Aviation</Text>
              </View>
            </ScrollView>
          </View>

        </View>
        {/* End: Categories section */}

        {/* Start: Latest Blogs section */}
        <View style={styles.postContainer}>

          <View style={styles.flexContainer}>
            <Text style={[styles.title, colorScheme === 'light' ? styles.titleDark : '']}>Latest Blogs</Text>
            <Text style={styles.viewAll} onPress={ () => props.navigation.navigate('Blogs') }>View All</Text>
          </View>

          <TouchableOpacity onPress={ () => props.navigation.navigate('Blog') }>
            <View style={styles.post}>
                <View>
                  <Image style={styles.postImage} source={{ uri: 'https://www.hindustantimes.com/ht-img/img/2023/09/17/550x309/Congress-president-Mallikarjun-Kharge-and-former-p_1694958285134.jpg' }} />
                  <Text style={styles.postHeading}>Congress leaders from Delhi, Punjab express reservation about poll pact with AAP</Text>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
                  <View style={{ flexDirection: "row", alignItems: "center"}}>
                    <Icon style={{ marginRight: 10 }} name="clock-o" size={20} color={colorScheme === 'light' ? '#8e8e8e' : 'white'} />
                    <Text>1 hours ago</Text>
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Icon style={{ marginRight: 10 }} name="bookmark" size={20} color={colorScheme === 'light' ? '#8e8e8e' : 'white'} />
                    <Text>Bookmark</Text>
                  </View>
                </View>
                <View style={styles.separator} />
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={ () => props.navigation.navigate('Blog') }>
            <View style={styles.post}>
                <View>
                  <Image style={styles.postImage} source={{ uri: 'https://www.hindustantimes.com/ht-img/img/2023/09/17/550x309/CRICKET-ASIA-2023-SRI-IND-ODI-49_1694953716417_1694953735233.jpg' }} />
                  <Text style={styles.postHeading}>Siraj storm wipes out Sri Lanka batting order as Team India lifts 8th Asia Cup title with crushing 10-wicket win</Text>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
                  <View style={{ flexDirection: "row", alignItems: "center"}}>
                    <Icon style={{ marginRight: 10 }} name="clock-o" size={20} color={colorScheme === 'light' ? '#8e8e8e' : 'white'} />
                    <Text>9 hours ago</Text>
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Icon style={{ marginRight: 10 }} name="bookmark" size={20} color={colorScheme === 'light' ? '#8e8e8e' : 'white'} />
                    <Text>Bookmark</Text>
                  </View>
                </View>
                <View style={styles.separator} />
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={ () => props.navigation.navigate('Blog') }>
            <View style={styles.post}>
                <View>
                  <Image style={styles.postImage} source={{ uri: 'https://www.hindustantimes.com/ht-img/img/2023/09/17/550x309/photo_2023-09-17_20-33-43_1694963217743_1694963222405.jpg' }} />
                  <Text style={styles.postHeading}>From florals to polka dots: Top 5 hottest prints that are all the rage this season</Text>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
                  <View style={{ flexDirection: "row", alignItems: "center"}}>
                    <Icon style={{ marginRight: 10 }} name="clock-o" size={20} color={colorScheme === 'light' ? '#8e8e8e' : 'white'} />
                    <Text>14 hours ago</Text>
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Icon style={{ marginRight: 10 }} name="bookmark" size={20} color={colorScheme === 'light' ? '#8e8e8e' : 'white'} />
                    <Text>Bookmark</Text>
                  </View>
                </View>
                <View style={styles.separator} />
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={ () => props.navigation.navigate('Blog') }>
            <View style={styles.post}>
                <View>
                  <Image style={styles.postImage} source={{ uri: 'https://www.hindustantimes.com/ht-img/img/2023/09/17/550x309/Screenshot_2023-09-17_220331_1694968421882_1694968429541.png' }} />
                  <Text style={styles.postHeading}>Picasso's masterpiece depicting his ‘golden muse’ could sell for over $120 million at auction</Text>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
                  <View style={{ flexDirection: "row", alignItems: "center"}}>
                    <Icon style={{ marginRight: 10 }} name="clock-o" size={20} color={colorScheme === 'light' ? '#8e8e8e' : '#828181'} />
                    <Text style={{ color: colorScheme === 'light' ? '#8e8e8e' : '#828181' }}>22 hours ago</Text>
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Icon style={{ marginRight: 10 }} name="bookmark" size={20} color={colorScheme === 'light' ? '#8e8e8e' : '#828181'} />
                    <Text style={{ color: colorScheme === 'light' ? '#8e8e8e' : '#828181' }}>Bookmark</Text>
                  </View>
                </View>
                <View style={styles.separator} />
            </View>
          </TouchableOpacity>

        </View>
        {/* End: Latest Blogs section */}

      </ScrollView>
    </SafeAreaView>
  );
}


