import { SafeAreaView, useColorScheme, Image, ScrollView } from "react-native";
import { Text, View } from "../components/Themed";
import Icon from 'react-native-vector-icons/FontAwesome';
import Carousel from 'react-native-snap-carousel';
import { sliderWidth, itemWidth } from '../styles/SliderEntry.style';
import SliderEntry from '../components/SliderEntry';
import { ENTRIES1 } from '../static/entries';
import { scrollInterpolators, animatedStyles } from '../utils/animations';
import styles from "./blogs.style";
import IconBack from 'react-native-vector-icons/Ionicons';

const renderItem = ({ item, index }) => {
  return <SliderEntry data={item} even={(index + 1) % 2 === 0} />;
}

const refNumber = 3;

export default function Blogs({navigation})
{
  const colorScheme = useColorScheme();

  return (
    <SafeAreaView style={[{backgroundColor: colorScheme === 'light' ? '#f4f4f4' : 'black'}]}>
      <ScrollView showsVerticalScrollIndicator={false}>

        {/* Start: Header section */}
        <View style={[styles.header, {borderBottomColor: colorScheme === 'light' ? '#eaeaea' : '#515151'}]}>
          <IconBack name="arrow-back-outline" size={20} color={colorScheme === 'light' ? '#8e8e8e' : 'white'} onPress={ () => navigation.navigate('Root') } />
          <Text style={[styles.title, colorScheme === 'light' ? styles.titleDark : '']} onPress={ () => navigation.navigate('Root') }>Blogs</Text>
        </View>
        {/* End: Header section */}

        {/* Start: Latest Blogs section */}
        <View style={styles.postContainer}>

          <View style={styles.post}>
              <View>
                <Image style={styles.postImage} source={{ uri: 'https://www.hindustantimes.com/ht-img/img/2023/10/07/550x309/FILES-DENMARK-DRUGS-OBESITY-ECONOMY-1_1696688198817_1696688268301.jpg' }} />
                <Text style={styles.postHeading}>Is Ozempic causing people to buy less food? Walmart CEO says yes</Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
                <View style={{ flexDirection: "row", alignItems: "center"}}>
                  <Icon style={{ marginRight: 10 }} name="clock-o" size={20} color={colorScheme === 'light' ? '#8e8e8e' : '#828181'} />
                  <Text style={{ color: colorScheme === 'light' ? '#8e8e8e' : '#828181' }}>22 hours ago</Text>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Icon style={{ marginRight: 10 }} name="bookmark" size={20} color={colorScheme === 'light' ? '#8e8e8e' : '#828181'} />
                  <Text style={{ color: colorScheme === 'light' ? '#8e8e8e' : '#828181' }}>Bookmark</Text>
                </View>
              </View>
              <View style={styles.separator} />
          </View>

          <View style={styles.post}>
              <View>
                <Image style={styles.postImage} source={{ uri: 'https://www.hindustantimes.com/ht-img/img/2023/10/07/550x309/ISRAEL-PALESTINIANS--219_1696695563902_1696695593304.JPG' }} />
                <Text style={styles.postHeading}>Video: Israel airstrike flattens high-rise building that housed Hamas' offices</Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
                <View style={{ flexDirection: "row", alignItems: "center"}}>
                  <Icon style={{ marginRight: 10 }} name="clock-o" size={20} color={colorScheme === 'light' ? '#8e8e8e' : '#828181'} />
                  <Text style={{ color: colorScheme === 'light' ? '#8e8e8e' : '#828181' }}>22 hours ago</Text>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Icon style={{ marginRight: 10 }} name="bookmark" size={20} color={colorScheme === 'light' ? '#8e8e8e' : '#828181'} />
                  <Text style={{ color: colorScheme === 'light' ? '#8e8e8e' : '#828181' }}>Bookmark</Text>
                </View>
              </View>
              <View style={styles.separator} />
          </View>

          <View style={styles.post}>
              <View>
                <Image style={styles.postImage} source={{ uri: 'https://www.hindustantimes.com/ht-img/img/2023/10/07/550x309/ISRAEL-NETANYAHU--1_1689487148635_1696708017507.JPG' }} />
                <Text style={styles.postHeading}>Israel attacks: PM Netanyahu vows 'mighty vengeance' against Hamas</Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
                <View style={{ flexDirection: "row", alignItems: "center"}}>
                  <Icon style={{ marginRight: 10 }} name="clock-o" size={20} color={colorScheme === 'light' ? '#8e8e8e' : '#828181'} />
                  <Text style={{ color: colorScheme === 'light' ? '#8e8e8e' : '#828181' }}>22 hours ago</Text>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Icon style={{ marginRight: 10 }} name="bookmark" size={20} color={colorScheme === 'light' ? '#8e8e8e' : '#828181'} />
                  <Text style={{ color: colorScheme === 'light' ? '#8e8e8e' : '#828181' }}>Bookmark</Text>
                </View>
              </View>
              <View style={styles.separator} />
          </View>

          <View style={styles.post}>
              <View>
                <Image style={styles.postImage} source={{ uri: 'https://www.hindustantimes.com/ht-img/img/2023/10/08/550x309/Gator_1696727677488_1696727678144.jpg' }} />
                <Text style={styles.postHeading}>Fishermen capture 13-foot-long, 680-pound alligator in Texas after 20 years</Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
                <View style={{ flexDirection: "row", alignItems: "center"}}>
                  <Icon style={{ marginRight: 10 }} name="clock-o" size={20} color={colorScheme === 'light' ? '#8e8e8e' : '#828181'} />
                  <Text style={{ color: colorScheme === 'light' ? '#8e8e8e' : '#828181' }}>22 hours ago</Text>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Icon style={{ marginRight: 10 }} name="bookmark" size={20} color={colorScheme === 'light' ? '#8e8e8e' : '#828181'} />
                  <Text style={{ color: colorScheme === 'light' ? '#8e8e8e' : '#828181' }}>Bookmark</Text>
                </View>
              </View>
              <View style={styles.separator} />
          </View>

          <View style={styles.post}>
              <View>
                <Image style={styles.postImage} source={{ uri: 'https://www.hindustantimes.com/ht-img/img/2023/10/07/550x309/PTI10-05-2023-000193B-0_1696710485028_1696710796226.jpg' }} />
                <Text style={styles.postHeading}>Shivraj Chouhan’s CM question at rally in MP triggers unease in BJP</Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
                <View style={{ flexDirection: "row", alignItems: "center"}}>
                  <Icon style={{ marginRight: 10 }} name="clock-o" size={20} color={colorScheme === 'light' ? '#8e8e8e' : '#828181'} />
                  <Text style={{ color: colorScheme === 'light' ? '#8e8e8e' : '#828181' }}>22 hours ago</Text>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Icon style={{ marginRight: 10 }} name="bookmark" size={20} color={colorScheme === 'light' ? '#8e8e8e' : '#828181'} />
                  <Text style={{ color: colorScheme === 'light' ? '#8e8e8e' : '#828181' }}>Bookmark</Text>
                </View>
              </View>
              <View style={styles.separator} />
          </View>

          <View style={styles.post}>
              <View>
                <Image style={styles.postImage} source={{ uri: 'https://www.hindustantimes.com/ht-img/img/2023/10/08/550x309/IAF_1696730307428_1696730317744.jpeg' }} />
                <Text style={styles.postHeading}>Woman officer to lead IAF Day parade for the first time today</Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
                <View style={{ flexDirection: "row", alignItems: "center"}}>
                  <Icon style={{ marginRight: 10 }} name="clock-o" size={20} color={colorScheme === 'light' ? '#8e8e8e' : '#828181'} />
                  <Text style={{ color: colorScheme === 'light' ? '#8e8e8e' : '#828181' }}>22 hours ago</Text>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Icon style={{ marginRight: 10 }} name="bookmark" size={20} color={colorScheme === 'light' ? '#8e8e8e' : '#828181'} />
                  <Text style={{ color: colorScheme === 'light' ? '#8e8e8e' : '#828181' }}>Bookmark</Text>
                </View>
              </View>
              <View style={styles.separator} />
          </View>

          <View style={styles.post}>
              <View>
                <Image style={styles.postImage} source={{ uri: 'https://www.hindustantimes.com/ht-img/img/2023/10/08/550x309/ISRAEL-PALESTINIANS--4_1696725687005_1696725727408.JPG' }} />
                <Text style={styles.postHeading}>Israel-Hamas war: Netanyahu's ‘get out’ warning for Gaza residents; militants kill 300 Israelis. Latest updates</Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
                <View style={{ flexDirection: "row", alignItems: "center"}}>
                  <Icon style={{ marginRight: 10 }} name="clock-o" size={20} color={colorScheme === 'light' ? '#8e8e8e' : '#828181'} />
                  <Text style={{ color: colorScheme === 'light' ? '#8e8e8e' : '#828181' }}>22 hours ago</Text>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Icon style={{ marginRight: 10 }} name="bookmark" size={20} color={colorScheme === 'light' ? '#8e8e8e' : '#828181'} />
                  <Text style={{ color: colorScheme === 'light' ? '#8e8e8e' : '#828181' }}>Bookmark</Text>
                </View>
              </View>
              <View style={styles.separator} />
          </View>

          <View style={styles.post}>
              <View>
                <Image style={styles.postImage} source={{ uri: 'https://c.ndtvimg.com/2023-10/76qo173g_hamas-israel-strikes-afp-_625x300_07_October_23.jpg' }} />
                <Text style={styles.postHeading}>"Nervous, Scared, In Touch With Embassy," Say Indian Students In Israel</Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
                <View style={{ flexDirection: "row", alignItems: "center"}}>
                  <Icon style={{ marginRight: 10 }} name="clock-o" size={20} color={colorScheme === 'light' ? '#8e8e8e' : '#828181'} />
                  <Text style={{ color: colorScheme === 'light' ? '#8e8e8e' : '#828181' }}>22 hours ago</Text>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Icon style={{ marginRight: 10 }} name="bookmark" size={20} color={colorScheme === 'light' ? '#8e8e8e' : '#828181'} />
                  <Text style={{ color: colorScheme === 'light' ? '#8e8e8e' : '#828181' }}>Bookmark</Text>
                </View>
              </View>
              <View style={styles.separator} />
          </View>

          <View style={styles.post}>
              <View>
                <Image style={styles.postImage} source={{ uri: 'https://www.hindustantimes.com/ht-img/img/2023/09/17/550x309/Congress-president-Mallikarjun-Kharge-and-former-p_1694958285134.jpg' }} />
                <Text style={styles.postHeading}>Congress leaders from Delhi, Punjab express reservation about poll pact with AAP</Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
                <View style={{ flexDirection: "row", alignItems: "center"}}>
                  <Icon style={{ marginRight: 10 }} name="clock-o" size={20} color={colorScheme === 'light' ? '#8e8e8e' : 'white'} />
                  <Text>1 hours ago</Text>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Icon style={{ marginRight: 10 }} name="bookmark" size={20} color={colorScheme === 'light' ? '#8e8e8e' : 'white'} />
                  <Text>Bookmark</Text>
                </View>
              </View>
              <View style={styles.separator} />
          </View>

          <View style={styles.post}>
              <View>
                <Image style={styles.postImage} source={{ uri: 'https://www.hindustantimes.com/ht-img/img/2023/09/17/550x309/CRICKET-ASIA-2023-SRI-IND-ODI-49_1694953716417_1694953735233.jpg' }} />
                <Text style={styles.postHeading}>Siraj storm wipes out Sri Lanka batting order as Team India lifts 8th Asia Cup title with crushing 10-wicket win</Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
                <View style={{ flexDirection: "row", alignItems: "center"}}>
                  <Icon style={{ marginRight: 10 }} name="clock-o" size={20} color={colorScheme === 'light' ? '#8e8e8e' : 'white'} />
                  <Text>9 hours ago</Text>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Icon style={{ marginRight: 10 }} name="bookmark" size={20} color={colorScheme === 'light' ? '#8e8e8e' : 'white'} />
                  <Text>Bookmark</Text>
                </View>
              </View>
              <View style={styles.separator} />
          </View>

          <View style={styles.post}>
              <View>
                <Image style={styles.postImage} source={{ uri: 'https://www.hindustantimes.com/ht-img/img/2023/09/17/550x309/photo_2023-09-17_20-33-43_1694963217743_1694963222405.jpg' }} />
                <Text style={styles.postHeading}>From florals to polka dots: Top 5 hottest prints that are all the rage this season</Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
                <View style={{ flexDirection: "row", alignItems: "center"}}>
                  <Icon style={{ marginRight: 10 }} name="clock-o" size={20} color={colorScheme === 'light' ? '#8e8e8e' : 'white'} />
                  <Text>14 hours ago</Text>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Icon style={{ marginRight: 10 }} name="bookmark" size={20} color={colorScheme === 'light' ? '#8e8e8e' : 'white'} />
                  <Text>Bookmark</Text>
                </View>
              </View>
              <View style={styles.separator} />
          </View>

          <View style={styles.post}>
              <View>
                <Image style={styles.postImage} source={{ uri: 'https://www.hindustantimes.com/ht-img/img/2023/09/17/550x309/Screenshot_2023-09-17_220331_1694968421882_1694968429541.png' }} />
                <Text style={styles.postHeading}>Picasso's masterpiece depicting his ‘golden muse’ could sell for over $120 million at auction</Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
                <View style={{ flexDirection: "row", alignItems: "center"}}>
                  <Icon style={{ marginRight: 10 }} name="clock-o" size={20} color={colorScheme === 'light' ? '#8e8e8e' : '#828181'} />
                  <Text style={{ color: colorScheme === 'light' ? '#8e8e8e' : '#828181' }}>22 hours ago</Text>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Icon style={{ marginRight: 10 }} name="bookmark" size={20} color={colorScheme === 'light' ? '#8e8e8e' : '#828181'} />
                  <Text style={{ color: colorScheme === 'light' ? '#8e8e8e' : '#828181' }}>Bookmark</Text>
                </View>
              </View>
              <View style={styles.separator} />
          </View>



        </View>
        {/* End: Latest Blogs section */}

      </ScrollView>
    </SafeAreaView>
  );
}


