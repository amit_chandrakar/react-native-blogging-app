import { SafeAreaView, useColorScheme, Image, ScrollView } from "react-native";
import { Text, View } from "../components/Themed";
import Carousel from 'react-native-snap-carousel';
import { sliderWidth, itemWidth } from '../styles/SliderEntry.style';
import SliderEntry from '../components/SliderEntry';
import { ENTRIES1 } from '../static/entries';
import { scrollInterpolators, animatedStyles } from '../utils/animations';
import styles from "./categories.style";
import Icon from 'react-native-vector-icons/Ionicons';

function Categories({ navigation })
{
  const colorScheme = useColorScheme();

  return (
    <SafeAreaView style={[{backgroundColor: colorScheme === 'light' ? '#f4f4f4' : 'black'}]}>
      <ScrollView showsVerticalScrollIndicator={false}>

        {/* Start: Header section */}
        <View
          style={[styles.header, {borderBottomColor: colorScheme === 'light' ? '#eaeaea' : '#515151'}]}
        >
          <Icon name="arrow-back-outline" size={20} color={colorScheme === 'light' ? '#8e8e8e' : 'white'} onPress={ () => navigation.navigate('Root') } />
          <Text style={[styles.title, colorScheme === 'light' ? styles.titleDark : '']} onPress={ () => navigation.navigate('Root') }>Categories</Text>
        </View>
        {/* End: Header section */}

        {/* Start: Categories */}
        <View style={styles.container}>
          {/* <View style={styles.flexContainer}>
            <Text style={styles.title}>Categories</Text>
          </View> */}

          <View style={styles.categoryContainer}>

            <View style={styles.category}>
              <Image source={{ uri: 'https://wallpaperaccess.com/full/552032.jpg' }} style={styles.categoryImage} />
              <Text style={[styles.categoryText, colorScheme === 'light' ? styles.categoryTextDark : '']}>Sports</Text>
            </View>

            <View style={styles.category}>
              <Image source={{ uri: 'https://images.unsplash.com/photo-1555848962-6e79363ec58f?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Nnx8cG9saXRpY3N8ZW58MHx8MHx8fDA%3D&w=1000&q=80' }} style={styles.categoryImage} />
              <Text style={[styles.categoryText, colorScheme === 'light' ? styles.categoryTextDark : '']}>Politics</Text>
            </View>

            <View style={styles.category}>
              <Image source={{ uri: 'https://wallpapercave.com/wp/wp9283871.jpg' }} style={styles.categoryImage} />
              <Text style={[styles.categoryText, colorScheme === 'light' ? styles.categoryTextDark : '']}>Aviation</Text>
            </View>

            <View style={styles.category}>
              <Image source={{ uri: 'https://wallpaperaccess.com/full/2222730.jpg' }} style={styles.categoryImage} />
              <Text style={[styles.categoryText, colorScheme === 'light' ? styles.categoryTextDark : '']}>Entertainment</Text>
            </View>

            <View style={styles.category}>
              <Image source={{ uri: 'https://images.pexels.com/photos/236171/pexels-photo-236171.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1' }} style={styles.categoryImage} />
              <Text style={[styles.categoryText, colorScheme === 'light' ? styles.categoryTextDark : '']}>Drama</Text>
            </View>

            <View style={styles.category}>
              <Image source={{ uri: 'https://images.pexels.com/photos/1327430/pexels-photo-1327430.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1' }} style={styles.categoryImage} />
              <Text style={[styles.categoryText, colorScheme === 'light' ? styles.categoryTextDark : '']}>Rock Band</Text>
            </View>

            <View style={styles.category}>
              <Image source={{ uri: 'https://images.pexels.com/photos/1298601/pexels-photo-1298601.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1' }} style={styles.categoryImage} />
              <Text style={[styles.categoryText, colorScheme === 'light' ? styles.categoryTextDark : '']}>Gaming</Text>
            </View>

            <View style={styles.category}>
              <Image source={{ uri: 'https://images.pexels.com/photos/210600/pexels-photo-210600.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1' }} style={styles.categoryImage} />
              <Text style={[styles.categoryText, colorScheme === 'light' ? styles.categoryTextDark : '']}>Finance</Text>
            </View>

            <View style={styles.category}>
              <Image source={{ uri: 'https://images.pexels.com/photos/2102416/pexels-photo-2102416.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1' }} style={styles.categoryImage} />
              <Text style={[styles.categoryText, colorScheme === 'light' ? styles.categoryTextDark : '']}>Technology</Text>
            </View>

            <View style={styles.category}>
              <Image source={{ uri: 'https://images.pexels.com/photos/3314294/pexels-photo-3314294.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1' }} style={styles.categoryImage} />
              <Text style={[styles.categoryText, colorScheme === 'light' ? styles.categoryTextDark : '']}>Social Media</Text>
            </View>

          </View>
        </View>

      </ScrollView>
    </SafeAreaView>
  )
}

export default Categories