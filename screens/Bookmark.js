import { SafeAreaView, useColorScheme, Image, ScrollView, TextInput } from "react-native";
import { Text, View } from "../components/Themed";
import styles from "./bookmark.style";
import Icon from 'react-native-vector-icons/Ionicons';

export default function Bookmark()
{
  const colorScheme = useColorScheme();

  return (
    <SafeAreaView style={[ styles.container, {backgroundColor: colorScheme === 'light' ? '#f4f4f4' : 'black'}]}>
      <Text style={[styles.message, colorScheme === 'light' ? styles.titleDark : '']}>You are not logged in. Please login first.</Text>
      <Text style={[styles.loginBtn, colorScheme === 'light' ? styles.titleDark : '']}>Login</Text>
    </SafeAreaView>
  );
}